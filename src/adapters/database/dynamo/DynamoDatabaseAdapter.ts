import 'reflect-metadata';
import { DatabaseAdapter } from '../DatabaseAdapter';
import { CONSTANTS, TYPES } from '../../../utils/Constants';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { inject, injectable } from 'inversify';
import { Logger } from '../../../utils/logger/Logger';

@injectable()
export class DynamoDatabaseAdapter implements DatabaseAdapter {
    private documentClient = new DocumentClient();

    constructor(@inject(TYPES.Logger) private LOGGER: Logger) {}

    async save(date: string, isMutant: boolean): Promise<void> {
        const parameters: DocumentClient.PutItemInput = {
            TableName: CONSTANTS.TABLE_NAME,
            Item: {
                DATA_TYPE: CONSTANTS.DATA_TYPE,
                DATE: date,
                IS_MUTANT: isMutant,
            },
        };
        this.LOGGER.info('Save in dynamo', parameters);
        await this.documentClient.put(parameters).promise();
    }
}
