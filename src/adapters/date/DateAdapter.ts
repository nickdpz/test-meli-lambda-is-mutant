export interface DateAdapter {
    getCurrentDateUTC(): string;
}
