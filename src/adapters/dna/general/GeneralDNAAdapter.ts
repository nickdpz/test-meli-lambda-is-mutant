import 'reflect-metadata';
import { DNAAdapter } from '../DNAAdapter';
import { injectable } from 'inversify';
import { RequestApiGWModel } from '../../../models/RequestApiGWModel';
import { DNAModel } from '../../../models/DNAModel';

@injectable()
export class GeneralDNAAdapter implements DNAAdapter {
    isMutant(dna: RequestApiGWModel): boolean {
        let count = 0;
        count = this.verifyDNARepeat(dna);
        if (count > 1) {
            return true;
        }
        const dnaArray = this.getDNAArray(dna);
        count = count + this.verifyDNARepeat(this.transposeMatrix(dnaArray));
        if (count > 1) {
            return true;
        }
        count = count + this.verifyDNARepeat(this.getDiagonals(dnaArray));
        if (count > 1) {
            return true;
        }
        return false;
    }

    private verifyDNARepeat(dna: RequestApiGWModel): number {
        const chainsMutant = dna.dna.filter((item) => /([ACGT])\1{3}/.test(item));
        console.log('Filter', chainsMutant);
        return chainsMutant.length;
    }

    private getDNAArray(dna: RequestApiGWModel): DNAModel {
        const array = [];
        for (const iterator of dna.dna) {
            array.push(Array.from(iterator));
        }
        return { dna: array };
    }

    private transposeMatrix(matrix: DNAModel): RequestApiGWModel {
        const result = matrix.dna.reduce(
            (r: [], a: []) => a.map((v, i) => [...(r[i] || []), v]),
            []
        );
        return { dna: result.map((item: []) => item.join('')) };
    }

    private getDiagonals(matrix: DNAModel): RequestApiGWModel {
        const threshold = 4;
        const len = matrix.dna.length;
        const max = len - threshold;
        const dna: string[] = [];
        let aux = '';
        for (let x = 0; x <= max; x++) {
            aux = '';
            for (let y = 0; y < len - x; y++) {
                aux = `${aux}${matrix.dna[y][x + y]}`;
            }
            dna.push(aux);
        }
        for (let x = len - 1; x > max; x--) {
            aux = '';
            for (let y = 0; y <= x; y++) {
                aux = `${aux}${matrix.dna[y][x - y]}`;
            }
            dna.push(aux);
        }
        return { dna };
    }
}
