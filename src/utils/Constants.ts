export const HTTP_CODES = {
    OK: 200,
    UNAUTHORIZED: 403,
    ERROR_REQUEST: 400,
    ERROR_INTERNAL: 500,
};

export const CONSTANTS = {
    LOG_LEVEL: 'debug',
    TIMEZONE: 'America/Bogota',
    TABLE_NAME: `meli-${process.env.NODE_ENV}-mutants-records-table`,
    DATA_TYPE: 'RECORD',
};

export const TYPES = {
    MainPresenter: Symbol.for('MainPresenter'),
    MainService: Symbol.for('MainService'),
    MainController: Symbol.for('MainController'),
    Logger: Symbol.for('Logger'),
    DatabaseAdapter: Symbol.for('DatabaseAdapter'),
    DateAdapter: Symbol.for('DateAdapter'),
    DNAAdapter: Symbol.for('DNAAdapter'),
};
