import 'reflect-metadata';
import { RequestApiGWModel } from '../../src/models/RequestApiGWModel';
import { MainService } from '../../src/services/MainService';
import { MainServiceImpl } from '../../src/services/MainServiceImpl';
import { WinstonLogger } from '../../src/utils/logger/winston/WinstonLogger';
import { ApiGWPresenter } from '../../src/presenters/apigw/ApiGWPresenter';
import { LuxonDateAdapter } from '../../src/adapters/date/luxon/LuxonDateAdapter';

const EVENT_SUCCESS: RequestApiGWModel = {
    dna: ['ATGCGA', 'CAGTGC', 'TTATGC', 'AGAAGG', 'CCCCTA', 'TCACTG'],
};

describe('MainService Test Suite', () => {
    const DatabaseAdapterSpy = jasmine.createSpyObj('DatabaseAdapter', ['save']);
    const saveDatabaseMock = DatabaseAdapterSpy.save as jasmine.Spy;

    const DNAAdapterSpy = jasmine.createSpyObj('DNAAdapter', ['isMutant']);
    const isMutantDNAAdapterMock = DNAAdapterSpy.isMutant as jasmine.Spy;

    let service: MainService;

    beforeEach(() => {
        service = new MainServiceImpl(
            DNAAdapterSpy,
            DatabaseAdapterSpy,
            new LuxonDateAdapter(),
            new ApiGWPresenter(),
            new WinstonLogger()
        );
    });

    it('processData should be return with response Mutant', async () => {
        isMutantDNAAdapterMock.withArgs(EVENT_SUCCESS).and.returnValue(true);
        saveDatabaseMock.and.resolveTo();
        await expectAsync(service.processData(EVENT_SUCCESS)).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 200 })
        );
    });

    it('processData should be return with response Human', async () => {
        isMutantDNAAdapterMock.withArgs(EVENT_SUCCESS).and.returnValue(false);
        saveDatabaseMock.and.resolveTo();
        await expectAsync(service.processData(EVENT_SUCCESS)).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 403 })
        );
    });

    it('processData should be reject with INTERNAL ERROR cause by save Dynamo', async () => {
        isMutantDNAAdapterMock.withArgs(EVENT_SUCCESS).and.returnValue(true);
        saveDatabaseMock.and.rejectWith(new Error('INTERNAL ERROR'));
        await expectAsync(service.processData(EVENT_SUCCESS)).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 500 })
        );
    });
});
