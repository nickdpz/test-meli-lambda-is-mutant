import 'reflect-metadata';
import { ApiGWPresenter } from '../../../src/presenters/apigw/ApiGWPresenter';
import { MainPresenter } from '../../../src/presenters/MainPresenter';

describe('MainPresenter Test Suite', () => {
    let presenter: MainPresenter;
    beforeEach(() => {
        presenter = new ApiGWPresenter();
    });
    it('generateMutantResponse should return an object with status code 200', async () => {
        expect(presenter.generateMutantResponse()).toEqual(
            jasmine.objectContaining({ statusCode: 200 })
        );
    });
    it('generateHumanResponse should return an object with status code 403', async () => {
        expect(presenter.generateHumanResponse()).toEqual(
            jasmine.objectContaining({ statusCode: 403 })
        );
    });
    it('generateInternalErrorResponse should return an object with status code 500', async () => {
        expect(presenter.generateInternalErrorResponse('Internal error')).toEqual(
            jasmine.objectContaining({ statusCode: 500 })
        );
    });
    it('generateErrorRequestResponse should return an object with status code ', async () => {
        expect(presenter.generateErrorRequestResponse('Internal error')).toEqual(
            jasmine.objectContaining({ statusCode: 400 })
        );
    });
});
