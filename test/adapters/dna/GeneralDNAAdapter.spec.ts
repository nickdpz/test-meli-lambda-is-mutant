import { GeneralDNAAdapter } from '../../../src/adapters/dna/general/GeneralDNAAdapter';
import { DNAAdapter } from '../../../src/adapters/dna/DNAAdapter';
import { RequestApiGWModel } from '../../../src/models/RequestApiGWModel';

const DNAMutantVertical: RequestApiGWModel = {
    dna: ['ATGCGA', 'CAGTGC', 'TTATGC', 'AGAAGG', 'CCACTC', 'TCACTG'],
};

const DNAMutantVerticalHorizontal: RequestApiGWModel = {
    dna: ['ATGCGA', 'CAGTGC', 'TTATGC', 'AGAAGG', 'CCCCTA', 'TCACTG'],
};

const DNAMutantHorizontal: RequestApiGWModel = {
    dna: ['ATGCGA', 'CAGTGC', 'TTATGC', 'AGAAAA', 'CCCCTA', 'TCACTG'],
};

const DNAMutantX1: RequestApiGWModel = {
    dna: ['AAGCGA', 'CAATGC', 'TTAATC', 'TGCAAT', 'CTCTAA', 'TCACTG'],
};

const DNAHuman: RequestApiGWModel = {
    dna: ['ATGCGA', 'CAGTGC', 'TTCTGC', 'AGTAAA', 'CCTCTA', 'TCACTG'],
};

describe('GeneralDNAAdapter Test Suite', () => {
    let generalDNA: DNAAdapter;
    beforeEach(() => {
        generalDNA = new GeneralDNAAdapter();
    });

    it('generalDNA should return true with horizontal', () => {
        expect(generalDNA.isMutant(DNAMutantHorizontal)).toEqual(true);
    });

    it('generalDNA should return true with vertical', () => {
        expect(generalDNA.isMutant(DNAMutantVertical)).toEqual(true);
    });

    it('generalDNA should return true with vertical horizontal', () => {
        expect(generalDNA.isMutant(DNAMutantVerticalHorizontal)).toEqual(true);
    });

    it('generalDNA should return true with vertical horizontal', () => {
        expect(generalDNA.isMutant(DNAMutantX1)).toEqual(true);
    });

    it('generalDNA should return false', () => {
        expect(generalDNA.isMutant(DNAHuman)).toEqual(false);
    });
});
