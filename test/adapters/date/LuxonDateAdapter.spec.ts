import { LuxonDateAdapter } from '../../../src/adapters/date/luxon/LuxonDateAdapter';
import { DateAdapter } from '../../../src/adapters/date/DateAdapter';

describe('LuxonDateAdapter Test Suite', () => {
    let luxonDateAdapter: DateAdapter;
    beforeEach(() => {
        luxonDateAdapter = new LuxonDateAdapter();
    });
    it('getCurrentDateUTC should return currentDay date', () => {
        const expectedDate = luxonDateAdapter.getCurrentDateUTC(),
            currentDay = new Date();
        console.log('getCurrentDateUTC', expectedDate);
        const currentDayString = currentDay.toLocaleString('eu-ES', {
            timeZone: 'UTC',
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        });
        console.log('Date ()', currentDayString);
        expect(expectedDate.split('T')[0]).toEqual(
            currentDayString.slice(0, 10).replace(/\//g, '-')
        );
    });
});
